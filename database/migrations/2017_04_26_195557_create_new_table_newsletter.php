<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTableNewsletter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletter', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 40);
            $table->text('description');
            $table->timestamps();
        });

        DB::table('newsletter')->insert([
            [
                'name'=>'Free credit',
                'description'=>'Get free credits'
            ],
            [
                'name'=>'Cheaper price',
                'description'=>'Cheapest items'
            ],
            [
                'name'=>'Sales of the month',
                'description'=>'Sales on produts'
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsletter');
    }
}
