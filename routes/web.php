<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// home
Route::get('/', 'SubscriptionController@create');
// dashboard
Route::get('/dashboard', 'AdminController@dashboard');

// create item form
Route::get('/subscription/create', 'SubscriptionController@create');
// create item action - store an item
Route::post('/subscription', 'SubscriptionController@store');

// update item form - show form for the update
Route::get('/subscription/{id}/edit', 'SubscriptionController@edit');
// update item action - update action
Route::put('/subscription/{id}', 'SubscriptionController@update');

// read item - show an item
Route::get('/subscription/{id}', 'SubscriptionController@show');

// show form unsubscribe
Route::get('/unsubscribe/{email?}', 'SubscriptionController@unsubscribe');
Route::get('/unsubscribed/{email}', 'SubscriptionController@unsubscribed');
Route::put('/storeunsubscribe', 'SubscriptionController@storeunsubscribe');


Route::group(['middleware'=>'admin'], function(){
    // read item - all models
    Route::get('/subscription', 'SubscriptionController@index');
    // delete item - delete an item
    Route::delete('/subscription/delete/{id}', 'SubscriptionController@destroy');
    // delete item - show form after deleting an item
    Route::get('/subscription/deleted/{id}', 'SubscriptionController@deleted');

    Route::resource('newsletter', 'NewsletterController');
    Route::get('/newsletter/{id}/deleted', 'NewsletterController@deleted');

    Route::resource('notification', 'NotificationController');
    Route::get('/notification', 'NotificationController@index');
    Route::get('/notification/{id}/deleted', 'NotificationController@deleted');
});

Auth::routes();

Route::get('/home', 'AdminController@dashboard');
Route::get('/back', 'AdminController@index')->name('admin.dashboard');
