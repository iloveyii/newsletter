<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $table = 'subscriber';
    public $fillable = ['name', 'email'];

    public function subscriptions()
    {
        return $this->hasMany('\App\Subscription');
    }

    public function hasSubscriptionForNewsletter($newsletter_id)
    {
        return Subscription::where(['subscriber_id'=>$this->id, 'newsletter_id'=>$newsletter_id])->exists();
    }
}
