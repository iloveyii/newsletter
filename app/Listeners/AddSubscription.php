<?php

namespace App\Listeners;

use App\Events\UserSubscribed;
use App\Subscriber;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Subscription;

class AddSubscription
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserSubscribed  $event
     * @return void
     */
    public function handle(UserSubscribed $event)
    {
        Subscription::where(['subscriber_id'=>$event->subscriber->id])->delete();

        foreach($event->newsletters as $newsletter_id) {
            $subscription = new Subscription;
            $subscription->subscriber_id = $event->subscriber->id;
            $subscription->newsletter_id = $newsletter_id;
            $subscription->save();
        }

        \Log::info('added subscription', ['user'=>$event->subscriber, 'newsletters'=>$event->newsletters]);
    }
}
