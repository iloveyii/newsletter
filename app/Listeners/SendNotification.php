<?php

namespace App\Listeners;

use App\Events\UserSubscribed;
use App\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotification
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserSubscribed  $event
     * @return void
     */
    public function handle(UserSubscribed $event)
    {
        $notification = new Notification();
        $notification->subscriber_id = $event->subscriber->id;
        $notification->save();

        \Log::info('send notification', ['user'=>$event->subscriber, 'newsletters'=>$event->newsletters]);
    }
}
