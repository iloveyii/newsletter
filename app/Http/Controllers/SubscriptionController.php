<?php

namespace App\Http\Controllers;

use App\Events\UserSubscribed;
use App\Newsletter;
use App\Subscriber;
use Illuminate\Http\Request;
use Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SubscriptionController extends Controller
{
    const SUBSCRIBED = 1;
    const UNSUBSCRIBED = 2;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Subscriber::orderBy('id', 'desc')->paginate(6);
        return view('subscription.index', ['models'=>$models]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Subscriber();
        $newsletters = Newsletter::all();
        return view('subscription.create', ['model'=>$model, 'newsletters'=>$newsletters]);
    }

    /**
     * Save the form data after validation
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|max:80',
            'email'=>'email|unique:subscriber,email',
            'newsletter'=>'required',
            'terms'=> 'required'
        ], [
            'email.unique'=>'This email is already subscribed.',
            'newsletter.required'=>'Please select at least one newsletter.',
            'terms.required' => 'Please accept our terms and conditions.'
        ]);

        $model = Subscriber::create($request->all());
        event(new UserSubscribed($model, $request->newsletter));
        $request->session()->flash('success', 'You subscribed to our newsletters successfully!');
        return redirect()->action('SubscriptionController@show', [
            'id' => $model->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @throws ModelNotFoundException
     */
    public function show($id)
    {
        $model = Subscriber::find($id);

        if(is_null($model)) {
            throw new ModelNotFoundException('This subscriber does not exist.');
        }

        return view('subscription.show', ['model'=>$model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Subscriber::find($id);
        $newsletters = Newsletter::all();

        if(is_null($model)) {
            die ('Not found');
        }

        return view('subscription.edit', ['model'=>$model, 'newsletters'=>$newsletters]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'=>'required|max:80',
            'email'=>'email',
            'newsletter'=>'required',
        ], [
            'newsletter.required'=>'Please select at least one newsletter.',
        ]);

        $model = Subscriber::find($id);
        $model->update($request->all());
        event(new UserSubscribed($model, $request->newsletter));
        $request->session()->flash('success', 'You subscription updated successfully!');

        return redirect()->action('SubscriptionController@show', [
            'id' => $id
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subscriber::destroy($id);

        Session::flash('success', 'The subscription deleted successfully!');

        return redirect()->action('SubscriptionController@deleted', [
            'id' => $id
        ]);
    }

    public function deleted($id)
    {
        return view('subscription.delete', [
            'id' => $id
        ]);
    }

    public function unsubscribe($email = null)
    {
        return view('subscription.unsubscribe', [
            'email'=>$email
        ]);
    }

    public function storeunsubscribe(Request $request)
    {
        $this->validate($request, [
            'email'=>'email',
        ]);

        $email = $request->get('email');
        $model = Subscriber::where('email', 'like', $email)->where('status', '=', self::SUBSCRIBED)->first();

        if(is_null($model)) {
            throw new ModelNotFoundException('User with email: ' . $email . ' does not exist, or already unsubscribed !');
        }

        $model->status = self::UNSUBSCRIBED;
        $model->update();

        $request->session()->flash('success', 'You are now unsubscribed!');
        return redirect()->action('SubscriptionController@unsubscribed', [
            'email' => $email
        ]);
    }

    public function unsubscribed($email)
    {
        return view('subscription.unsubscribed', [
            'email'=>$email
        ]);
    }
}
