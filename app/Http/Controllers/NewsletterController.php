<?php

namespace App\Http\Controllers;

use App\Newsletter;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Newsletter::orderBy('id', 'desc')->paginate(6);
        return view('newsletter.index', ['models'=>$models]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Newsletter();
        return view('newsletter.create', ['model'=>$model]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|max:80',
            'description'=>'required|max:400',
        ]);

        $model = Newsletter::create($request->all());
        $request->session()->flash('success', 'Newsletter created successfully!');

        if(config('app.debug')) {
            \Log::info('Newsletter_created', ['newsletter' => $model]);
        }

        return redirect()->action('NewsletterController@show', [
            'id' => $model->id
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @throws ModelNotFoundException
     */
    public function show($id)
    {
        $model = Newsletter::find($id);

        if(is_null($model)) {
            throw new ModelNotFoundException('This newsletter does not exist.');
        }

        return view('newsletter.show', ['model'=>$model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Newsletter::find($id);
        return view('newsletter.edit', ['model'=>$model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'=>'required|max:80',
            'description'=>'required|max:400',
        ]);

        Newsletter::find($id)->update($request->all());
        $request->session()->flash('success', 'Newsletter updated successfully!');

        return redirect()->action('NewsletterController@show', [
            'id' => $id
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Newsletter::destroy($id);

        Session::flash('success', 'The newsletter deleted successfully!');

        return redirect()->action('NewsletterController@deleted', [
            'id' => $id
        ]);
    }

    public function deleted($id)
    {
        return view('newsletter.delete', [
            'id' => $id
        ]);

    }
}
