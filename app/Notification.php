<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';
    public $fillable = ['subscriber_id'];

    public function subscriber()
    {
        return $this->belongsTo('\App\Subscriber');
    }
}
