<?php

namespace App\Events;

use App\Subscriber;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserSubscribed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $subscriber;
    public $newsletters;

    /**
     * Create a new event instance.
     *
     */
    public function __construct(Subscriber $subscriber, $newsletters)
    {
        $this->subscriber = $subscriber;
        $this->newsletters = $newsletters;
    }
}
