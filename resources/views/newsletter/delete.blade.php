@extends('layouts.admin')

@section('content')
    <div class="col-md-6 col-md-offset-3 background modal-content">
        @include('partial.message')
        <h3>Delete successful with id <?= $id ?> </h3>

        <br />
        <a href="/newsletter" class="btn btn-info btn-flat"><span class="glyphicon glyphicon-arrow-left "></span> Back</a>
    </div>
@endsection