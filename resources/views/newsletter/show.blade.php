@extends('layouts.admin')

@section('content')
    <div class="col-md-8 col-md-offset-2 background modal-content">
        @include('partial.message')
        <h3>Your Subscription !</h3>
        <hr />
        <div class="form-group">
            {!! Form::label('name', 'Name: ') !!}
            {!! Form::label('name', $model->name) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description: ') !!}
            {!! Form::label('description', $model->description) !!}
        </div>

        <br />

        <a href="/newsletter/<?=$model->id?>/edit" class="btn btn-success btn-flat"><span class="glyphicon glyphicon-pencil"></span> Update</a>
        <a href="/newsletter/" class="btn btn-info btn-flat"><span class="glyphicon glyphicon-arrow-left "></span> Back</a>
    </div>
    <br />

@endsection