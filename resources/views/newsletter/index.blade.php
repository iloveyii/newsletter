@extends('layouts.admin')

@section('content')
    <div class="col-md-8 col-md-offset-2 background modal-content">
        <h3><span class="glyphicon glyphicon-envelope text-warning"></span> &nbsp; All newsletters</h3>
        <br />

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
            <?php $c = 0; ?>
            <?php foreach($models as $model):?>
                <tr>
                    <th scope="row"><?= ++$c ?></th>
                    <td><?= $model->name ?></td>
                    <td><?= $model->description ?></td>
                    <td>
                        {!! Form::open(['action' => ['NewsletterController@destroy', $model->id], 'class'=>'form-inline', 'method'=>'delete']) !!}
                            <button type="submit" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                            <a href="/newsletter/<?=$model->id?>/edit" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-pencil"></span> Update</a>
                            <a href="/newsletter/<?=$model->id?>" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                        {!! Form::close() !!}
                    </td>
                </tr>
            <?php endforeach ?>
            </tbody>
        </table>

        <div class="text-center">
            <ul class="pagination pull-left">
                <a href="/newsletter/create" class="btn btn-success btn-flat"><span class="glyphicon glyphicon-plus-sign"></span> New</a>
                <a href="{{action('AdminController@dashboard')}}" class="btn btn-info btn-flat"><span class="glyphicon glyphicon-arrow-left "></span> Back</a>
            </ul>
            {!! $models->links() !!}
        </div>

    </div>
@endsection


