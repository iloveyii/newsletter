@extends('layouts.admin')

@section('content')
    <div class="col-md-6 col-md-offset-3 background modal-content">
        @include('partial.message')
        <h1>Update newsletter</h1>
        @include('newsletter._form')
    </div>
@endsection