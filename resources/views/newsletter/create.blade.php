@extends('layouts.admin')

@section('content')
    <div class="col-md-6 col-md-offset-3 background modal-content">
        @include('partial.message')
        <h2><span class="glyphicon glyphicon-envelope text-success"></span> Create a newsletter</h2>
        <br />
        @include('newsletter._form')
    </div>
@endsection
