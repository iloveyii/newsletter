{!! Form::open(['action' => $model->exists ? ['NewsletterController@update', $model->id] : 'NewsletterController@store', 'data-parsley-validate'=>'' ]) !!}

    {!! Form::hidden('_method', $model->exists ? 'PUT' : 'POST') !!}

    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', $model->name, ["placeholder"=>"Name", "class"=>"form-control", 'required'=>'']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textArea('description', $model->description, ["placeholder"=>"Description", "class"=>"form-control", 'required'=>'']) !!}
    </div>
    <hr />
    <button type="submit" class="btn btn-success btn-flat">

        @if($model->exists)
            <span class="glyphicon glyphicon-floppy-saved"></span>
            Update
        @else
            <span class="glyphicon glyphicon-floppy-disk"></span>
            Save
        @endif

    </button>
    <a href="/newsletter/" class="btn btn-info btn-flat"><span class="glyphicon glyphicon-arrow-left "></span> Back</a>

{!! Form::close() !!}