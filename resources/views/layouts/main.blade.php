<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Newsletter subscription</title>
    <!-- Css -->
    <link href="https://bootswatch.com/cosmo/bootstrap.css" rel="stylesheet">
    {{Html::style('css/parsley.css')}}
    {{Html::style('css/site.css')}}
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    {{ Html::script('js/parsley.min.js') }}
</head>
<body>
    <div class="wrapper">
        <section id="navigation">
            <div class="container-fullwidth">
                <!-- Static navbar -->
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="{{action('SubscriptionController@create')}}">Newsletter</a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <!-- Left Side Of Navbar -->
                            <ul class="nav navbar-nav">
                                    <li {{ (Request::is('subscription/create') ? 'class=active' : '') }}>
                                        <a href="{{action('SubscriptionController@create')}}">Subscribe</a>
                                    </li>
                                <li {{ (Request::is('unsubscribe') ? 'class=active' : '') }}><a href="{{action('SubscriptionController@unsubscribe')}}">Unsubscribe</a></li>
                            </ul>
                            <!-- Right Side Of Navbar -->
                            <ul class="nav navbar-nav navbar-right">
                                <!-- Authentication Links -->
                                @if (Auth::guest())
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                            <li><a href="/dashboard">Admin</a></li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div><!--/.container-fluid -->
                </nav>
            </div>
        </section>

        <section id="main">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   @yield('content')
                </div>
            </div>
        </div>
    </section>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">© SoftHem 2017</p>

            <p class="pull-right">Powered by <a href="http://www.softhem.se/" rel="external">SoftHem</a></p>
        </div>
    </footer>
</body>
</html>