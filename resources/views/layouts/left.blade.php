<?php
$directoryAsset = '/admin';
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=$directoryAsset?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Admin</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input name="q" class="form-control" placeholder="Search..." type="text">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <ul class="sidebar-menu">
            <li><a href="/"><i class="fa fa-home"></i>  <span>Home</span></a></li>
            <li><a href="/dashboard"><i class="fa fa-dashboard"></i>  <span>Dashboard</span></a></li>

            <li><a href="#"><i class="fa fa-share"></i>  <span>Admin</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="/newsletter"><i class="fa  fa-newspaper-o"></i>  <span>Newsletters</span></a></li>
                    <li><a href="/subscription"><i class="fa fa-user"></i>  <span>Subscriptions</span></a></li>
                    <li><a href="/notification"><i class="fa fa-bell"></i>  <span>Notifications</span></a></li>
                </ul>
            </li>

            <li>
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i>
                    <span>Logout</span>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>

        </ul>
    </section>

</aside>