<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Newsletter subscription</title>
    <!-- Css -->
    <link href="https://bootswatch.com/cosmo/bootstrap.css" rel="stylesheet">
    {{Html::style('admin/css/AdminLTE.min.css')}}
    {{Html::style('admin/css/site.css')}}
    {{Html::style('admin/css/dashcut.css')}}
    {{Html::style('admin/css/skins/_all-skins.min.css')}}
    {{Html::style('css/parsley.css')}}
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    {{ Html::script('js/parsley.min.js') }}
    {{ Html::script('admin/js/app.min.js') }}
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        @include('layouts.header')
        @include('layouts.left')
        @include('layouts.content')
    </div>
</body>
</html>