@extends('layouts.main')

@section('content')
    <div class="col-md-6 col-md-offset-3 background modal-content">
        @include('partial.message')
        <h2><span  class="text-danger glyphicon glyphicon-remove-circle"></span> Unsubscribe from our newsletter</h2>
        <br />
        {!! Form::open(['action' => 'SubscriptionController@storeunsubscribe', 'method'=>'put']) !!}

            <div class="form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::email('email', $email, ["placeholder"=>"Email", "class"=>"form-control"]) !!}
            </div>
            <br />

            <hr />
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-envelope"></span> Unsubscribe</button>
            <a href="{{action('SubscriptionController@create')}}" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left "></span> Back</a>
        {!! Form::close() !!}

    </div>
@endsection
