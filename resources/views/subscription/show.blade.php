@extends('layouts.main')

@section('content')
    <div class="col-md-8 col-md-offset-2 background modal-content">
        @include('partial.message')
        <h3>Thank you for subscribing with us</h3>
        <hr />
        <div class="form-group">
            {!! Form::label('name', 'Name: ') !!}
            {!! Form::label('name', $model->name) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email: ') !!}
            {!! Form::label('email', $model->email) !!}
        </div>

        <ul class="dotted">
            @foreach($model->subscriptions as $subscription)
                @if(! is_null($subscription))
                    <li>{{ $subscription->newsletter->name }} </li>
                @endif
            @endforeach
        </ul>
        <br />

        <a href="/subscription/<?=$model->id?>/edit" class="btn btn-success btn-flat"><span class="glyphicon glyphicon-pencil"></span> Update</a>
        <a href="{{action('SubscriptionController@create')}}" class="btn btn-info btn-flat"><span class="glyphicon glyphicon-arrow-left "></span> Back</a>
    </div>
    <br />

@endsection