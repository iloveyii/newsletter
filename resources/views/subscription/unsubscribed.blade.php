@extends('layouts.main')

@section('content')
    <div class="col-md-8 col-md-offset-2 background modal-content">
        @include('partial.message')
        <h2>Unsubscribed: </h2>
        <p>{{ $email }} is now unsubscribed from our newsletter. You will not receive emails about our newsletter.</p>
        <br />
        <hr />
        <a href="{{action('SubscriptionController@create')}}" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left "></span> Back</a>
    </div>


@endsection