@extends('layouts.main')

@section('content')
    <div class="col-md-6 col-md-offset-3 background modal-content">
        @include('partial.message')
        <h2> <span class="glyphicon glyphicon-plus-sign text-success"></span> Subscribe to our newsletter</h2>
        <br />
        {!! Form::open(['action' => 'SubscriptionController@store', 'data-parsley-validate'=>'']) !!}

            <div class="form-group">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', $model->name, ["placeholder"=>"Name", "class"=>"form-control", 'Required'=>'']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::email('email', $model->email, ["placeholder"=>"Email", "class"=>"form-control", 'Required'=>'']) !!}
            </div>

            @foreach($newsletters as $newsletter)
                <div class="checkbox">
                    <label>
                        {!! Form::checkbox("newsletter[$newsletter->id]", $newsletter->id, $model->hasSubscriptionForNewsletter($newsletter->id) ? 'checked' : '')!!}  {{ $newsletter->name }}
                    </label>
                </div>
            @endforeach
            <hr />

            <div class="checkbox">
                <label>
                    {!! Form::checkbox('terms', 'yes') !!} I have read and accepted the terms and conditions.
                </label>
            </div>
            <br />
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-envelope"></span> Subscribe</button>
            <a href="{{action('SubscriptionController@create')}}" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left "></span> Back</a>
        {!! Form::close() !!}

    </div>
@endsection
