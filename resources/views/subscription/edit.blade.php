@extends('layouts.main')

@section('content')
    <div class="col-md-6 col-md-offset-3 background modal-content">
        @include('partial.message')
        <h1>Subscribe to our newsletter</h1>
        {!! Form::open(['action' => ['SubscriptionController@update', $model->id], 'method'=>'put']) !!}

            <div class="form-group">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', $model->name, ["placeholder"=>"Name", "class"=>"form-control"]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', $model->email, ["placeholder"=>"Email", "class"=>"form-control"]) !!}
            </div>

            @foreach($newsletters as $newsletter)
                <div class="checkbox">
                    <label>
                        {!! Form::checkbox("newsletter[$newsletter->id]", $newsletter->id, $model->hasSubscriptionForNewsletter($newsletter->id) ? 'checked' : '')!!}  {{ $newsletter->name }}
                    </label>
                </div>
            @endforeach

            <br />
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-envelope"></span> Update</button>
            <a href="{{action('SubscriptionController@create')}}" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left "></span> Back</a>

        {!! Form::close() !!}
    </div>
@endsection