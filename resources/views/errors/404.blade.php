@extends('layouts.main')

@section('content')
    <div class="col-md-8 col-md-offset-2 background modal-content">
        <h2>Not Found: </h2>
        <p>{{ $message }}</p>
        <br />
        <hr />
        <a href="/subscription" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left "></span> Back</a>
    </div>


@endsection