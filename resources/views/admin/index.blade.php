@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div id="w0" class="small-box bg-purple"><div class="inner"><h3>90<sup style="font-size: 20px">%</sup></h3><p>Free Space</p></div><div class="icon"><i class="fa fa-cloud-download"></i></div><a class="small-box-footer" href="#">More info <i class="fa fa-arrow-circle-right"></i></a></div>        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div id="w1" class="small-box bg-green"><div class="inner"><h3>53<sup style="font-size: 20px">%</sup></h3><p>Bounce Rate</p></div><div class="icon"><i class="fa fa-stars-bars"></i></div><a class="small-box-footer" href="#">More info <i class="fa fa-arrow-circle-right"></i></a></div>        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div id="w2" class="small-box bg-teal"><div class="inner"><h3>44<sup style="font-size: 20px">%</sup></h3><p>User Registrations</p></div><div class="icon"><i class="fa fa-stars-bars"></i></div><a class="small-box-footer" href="#">More info <i class="fa fa-arrow-circle-right"></i></a></div>        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div id="w3" class="small-box bg-red"><div class="inner"><h3>65<sup style="font-size: 20px">%</sup></h3><p>Unique Visitors</p></div><div class="icon"><i class="fa fa-stars-bars"></i></div><a class="small-box-footer" href="#">More info <i class="fa fa-arrow-circle-right"></i></a></div>        </div>
        <!-- ./col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <a class="beginner" href="/newsletter">
                <i class="fa fa-newspaper-o"></i>
                <span><small>LIST</small>Newsletters</span>
            </a>
        </div>

        <div class="col-lg-3 col-xs-6">
            <a class="category" href="/subscription">
                <i class="fa fa-user"></i>
                <span><small>LIST</small>Subscriptions</span>
            </a>
        </div>

        <div class="col-lg-3 col-xs-6">
            <a class="advanced" href="/notification">
                <i class="fa fa-bell"></i>
                <span><small>LIST</small>Notifications</span>
            </a>
        </div>

        <div class="col-lg-3 col-xs-6">
            <a class="intermediate" href="/">
                <i class="fa fa-send"></i>
                <span><small>BACK</small>Frontend</span>
            </a>
        </div>

    </div>
    <!-- /.row -->
@endsection

