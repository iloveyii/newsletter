@extends('layouts.admin')

@section('content')
    <div class="col-md-8 col-md-offset-2 background modal-content">
        <h3><span class="glyphicon glyphicon-envelope text-warning"></span> &nbsp; All notifications</h3>
        <br />

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Email</th>
                    <th>Date sent</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
            <?php $c = 0; ?>
            <?php foreach($models as $model):?>
                <tr>
                    <th scope="row"><?= ++$c ?></th>
                    <td><?= isset($model->subscriber->email) ? $model->subscriber->email : 'NA' ?></td>
                    <td><?= $model->created_at ?></td>
                    <td>
                        {!! Form::open(['action' => ['NotificationController@destroy', $model->id], 'class'=>'form-inline', 'method'=>'delete']) !!}
                            <button type="submit" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            <?php endforeach ?>
            </tbody>
        </table>

        <div class="text-center">
            <ul class="pagination pull-left">
                <a href="{{action('AdminController@dashboard')}}" class="btn btn-info btn-flat"><span class="glyphicon glyphicon-arrow-left "></span> Back</a>
            </ul>
            {!! $models->links() !!}
        </div>

    </div>
@endsection

