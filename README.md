# Newsletter #

This is a small newsletter repo developed in Laravel 5.4.

### What is this repository for? ###

* Create an application written in PHP (preferably in Laravel). Visitors will be able to sign up for the newsletter with name, email and the ability to choose between three different newsletters (to select several at once). The visitor will be taken to a thank you page, and the information will be stored in a database. An
  admin should be able to log in and see all notified and delete one or more notifications. The visitors will be able to unsubscribe themselves in the frontend.
* Version 1.1
* [Live demo](http://newsletter.softhem.se/)

### How do I get set up? ###

* This application is developed in Laravel 5.4
* Configuration file is .env
* Dependencies are managed and installed by composer
* Database configuration file is .env
* Deployment instructions
* Installation
    1. Using git
        *   `git clone git@bitbucket.org:iloveyii/newsletter.git`
        OR
        * uncompress the attached zipped file
            `tar -xvf newsletter.tar.gz `
    2. Install dependencies
        `composer install`
    3. Config file
        `cp .env.example .env`
    4. edit the .env file to match your database name and credentials
    5. run migrations
        `php artisan migrate`
    6. generate key
        `php artisan key:generate`
    7. run web server
        `php artisan serve`
    8. subscribe to news letter by clicking subscribe
    9. Register a new user (admin by default) by clicking the link register on the menu, login!

### Who do I talk to? ###

* Repo owner or admin
* Hazrat Ali: ali.sweden19@yahoo.com
